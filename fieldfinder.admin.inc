<?php

/*Admin build fieldfinder*/
function fieldfinder_admin_settings($arg=NULL){
  $output=fieldfinder_admin_list();
  return $output;
}
/*Admin build fieldfinder Add*/
function fieldfinder_admin_add(){
  $output=drupal_get_form('fieldfinder_admin_add_form');
  return $output;
}
/*Admin build fieldfinder List -- edit*/
function fieldfinder_admin_edit($lid){
  $output=drupal_get_form('fieldfinder_admin_edit_form',$lid);
  return $output;
}
/*Admin build fieldfinder List -- delete*/
function fieldfinder_admin_del($lid){
  $output=drupal_get_form('fieldfinder_admin_del_form',$lid);
  return $output;
}
/*Admin build fieldfinder List -- edit field*/
/*Admin build fieldfinderedit -- manage fields*/
function fieldfinder_admin_field_edit($lid){
  $output='';
  $output .= drupal_get_form('fieldfinder_admin_field_edit_form',$lid);
  
  return $output;
}
/*Admin build fieldfinderedit -- manage group*/
function fieldfinder_admin_group($lid){
  $output='';
  $output .= drupal_get_form('fieldfinder_admin_group_form',$lid);
  $output .= drupal_get_form('fieldfinder_admin_group_list_form',$lid);
  return $output;
}

/*Admin build fieldfinderedit -- manage group -- add group*/
function fieldfinder_admin_add_form(&$form_state){
  //print_r ($form_state);

  //查找已存在的内容类型
  $all_node_type=fieldfinder_ui_node_type();

  //FieldFinder的名称
  $form['fieldfinder_node_type']['listname']=array(
		'#type'=>'textfield',
		'#title'=>t('FieldFinder name'),
		'#description'=>t('This is FieldFinder name'),
		'#required'=>TRUE,
		'#maxlength'=>32,
  );
  //输入出页面还是区块
  $form['fieldfinder_node_type']['listtype']=array(
		'#type'=>'radios',
		'#title'=>t('Output Type'),
		'#description'=>t('It is a Page or Block'),
		'#required'=>TRUE,
		'#options'=>array('block'=>t('block'),'page'=>t('page')),
		'#attributes'=>array(
		  'onclick'=>'fieldfinderShowPath(this)',
		),
		'#default_value'=>($form_state['post']['listtype'] == 'page')?'page':'block',
  );
  //如果选择的是输出页面,显示输入页面地址
  $form['fieldfinder_node_type']['page_path']=array(
		'#type'=>'textfield',
		'#title'=>t('Path'),
		'#prefix'=>($form_state['post']['listtype'] == 'page')?'<div id="fieldfinder-page-path">':'<div id="fieldfinder-page-path" style="display:none">',
		'#suffix'=>'</div>',
		'#description'=>t('The path or URL of this fieldfinder page. This must be an internal Drupal path such as mysearch/result'),
		'#field_prefix'=>url(NULL,array('absolute'=>TRUE)) .(variable_get('clean_url',0)?'':'?q='),
  );
  //选择要关注的内容类型
  $form['fieldfinder_node_type']['select_type']=array(
		'#type'=>'select',
		'#title'=>t('select node type'),
		'#default_value'=>$_POST['select_type'],
		'#options'=>$all_node_type,
  );
  //注释
  $form['fieldfinder_node_type']['description']=array(
		'#type'=>'textarea',
		'#title'=>t('description'),
  );
  //提交按钮
  $form['fieldfinder_node_type']['submit']=array(
		'#type'=>'submit',
		'#value'=>t('Save FieldFinder'),
		'#submit'=>array('fieldfinder_add_form_submit'),
  );

  return $form;
}

//编辑lid的Fieldfinder
function fieldfinder_admin_edit_form(&$form_state,$lid){

  //获取lid的结构
  $listinfo=fieldfinder_get_lists_instance($lid);

  //查找已存在的内容类型
  $all_node_type=fieldfinder_ui_node_type($lid);

  //FieldFinder的名称
  $form['fieldfinder_node_type']['lid']=array(
		'#type'=>'hidden',
		'#value'=>$listinfo['base']->lid,
  );
  $form['fieldfinder_node_type']['listname']=array(
		'#type'=>'textfield',
		'#title'=>t('FieldFinder name'),
		'#description'=>t('This is FieldFinder name'),
		'#required'=>TRUE,
		'#maxlength'=>32,
		'#default_value'=>$listinfo['base']->listname,
  );
  //输入出页面还是区块
  $form['fieldfinder_node_type']['listtype']=array(
		'#type'=>'radios',
		'#title'=>t('Output Type'),
		'#description'=>t('It is a Page or Block'),
		'#required'=>TRUE,
		'#options'=>array('block'=>'block','page'=>'page'),
		'#attributes'=>array(
		  'onclick'=>'fieldfinderShowPath(this)',
		),
		'#default_value'=>$listinfo['base']->list_type,
  );
  //如果选择的是输出页面,显示输入页面地址
  $form['fieldfinder_node_type']['page_path']=array(
		'#type'=>'textfield',
		'#title'=>t('Path'),
		'#prefix'=>($listinfo['base']->list_type == 'block')?'<div id="fieldfinder-page-path" style="display:none">':'<div id="fieldfinder-page-path">',
		'#suffix'=>'</div>',
		'#field_prefix'=>url(NULL,array('absolute'=>TRUE)) .(variable_get('clean_url',0)?'':'?q='),
		'#description'=>t('The path or URL of this fieldfinder page. This must be an internal Drupal path such as mysearch/result'),
		'#default_value'=>$listinfo['base']->page_path,
  );
  //选择要关注的内容类型
  $form['fieldfinder_node_type']['select_type']=array(
		'#type'=>'select',
		'#title'=>t('select node type'),
		'#default_value'=>$listinfo['base']->content_type,
		'#options'=>$all_node_type,
		'#disabled'=>TRUE,
  );
  //注释
  $form['fieldfinder_node_type']['description']=array(
		'#type'=>'textarea',
		'#title'=>t('description'),
		'#default_value'=>$listinfo['base']->description,
  );
  //提交按钮
  $form['fieldfinder_node_type']['submit']=array(
		'#type'=>'submit',
		'#value'=>t('Save FieldFinder'),
		'#submit'=>array('fieldfinder_edit_form_submit'),
  );
  //删除按钮
  $form['fieldfinder_node_type']['delete']=array(
		'#type'=>'submit',
		'#value'=>t('Delete FieldFinder'),
		'#submit'=>array('fieldfinder_edit_form_submit'),
  );

  return $form;
}
function fieldfinder_add_form_submit($form,&$form_state){
  //检查page_path
  if ($form_state['values']['listtype'] == 'page'  && empty($form_state['values']['page_path'])){
		form_set_error('path_undefined',t('Display Page uses path but path is undefined.') ,FALSE);
		return $form;
  }

  $result=db_query("INSERT INTO {fieldfinder_list} (listname, content_type, list_type, page_path, description) VALUES ('%s', '%s', '%s', '%s', '%s') ",
		$form_state['values']['listname'], $form_state['values']['select_type'], $form_state['values']['listtype'], $form_state['values']['page_path'], $form_state['values']['description']);
  if ($result){
		//call fieldfinder_menu;
		menu_rebuild();
		drupal_set_message(t('FieldFinder has been saved!'));
  }
}
function fieldfinder_edit_form_submit($form,&$form_state){
  if ($form_state['values']['op']=='Save FieldFinder'){
		//检查page_path
		if ($form_state['values']['listtype'] == 'page'  && empty($form_state['values']['page_path'])){
		  form_set_error('path_undefined',t('Display Page uses path but path is undefined.'),FALSE);
		  return $form;
		}

		$result=db_query("UPDATE {fieldfinder_list} SET listname = '%s', list_type = '%s', page_path = '%s', description ='%s' WHERE lid = %d ",$form_state['values']['listname'], $form_state['values']['listtype'], $form_state['values']['page_path'], $form_state['values']['description'], $form_state['values']['lid']);
		if ($result){
		  drupal_set_message(t('Changes have been saved!'));
		}
  }else if ($form_state['values']['op'] == 'Delete FieldFinder'){
		$form_state['redirect'] = 'admin/build/fieldfinderedit/'. $form_state['values']['lid'] . '/del';
		return;
  }
}
function fieldfinder_ui_node_type(){
  $db_string="SELECT type,name from {node_type} where module = 'node'";
  $result=db_query($db_string);
  $output=array();
  while ($node_type = db_fetch_object($result)){
		$output[$node_type->type]=$node_type->name;
  }
  return $output;
}

function fieldfinder_admin_list(){
  $rows=array();
  $header=array(t('Name'),t('Type'),t('Content_type'),array('data'=>t('Operations'),'colspan'=>'3'),);
  $output='';

  $results=db_query("SELECT lid,listname,content_type,list_type from {fieldfinder_list}");
  
  while ($result = db_fetch_object($results)){
		//print_r ($result);
		$row=array();
		$row[]=array('data'=>$result->listname);
		$row[]=array('data'=>$result->list_type);
		$row[]=array('data'=>$result->content_type);

		$row[]=array('data'=>l(t('Edit'),'admin/build/fieldfinderedit/' . $result->lid . '/edit'));
		$row[]=array('data'=>l(t('Edit field'),'admin/build/fieldfinderedit/' . $result->lid . '/editfield'));
		$row[]=array('data'=>l(t('Delete'),'admin/build/fieldfinderedit/' . $result->lid . '/del'));
		$rows[]=$row;
  }
  $output ='<div class="help">' . t('Below is a list of all the FieldFinders on your site.') . '</div>';
  $output .=theme('table', $header, $rows);
  $output .='<div class="fieldfinder-add-links">' . l('» Add a new fieldfinder', 'admin/build/fieldfinder/add') . '</div>';
  return $output;
}

function fieldfinder_admin_group_form(&$form_state, $lid){

  $form['fieldfinder_group_add']['lid']=array(
		'#type'=>'hidden',
		'#value'=>$lid,
  );
  $form['fieldfinder_group_add']['groupname']=array(
		'#type'=>'textfield',
		'#title'=>t('Group Name'),
		'#required'=>TRUE,
		'#maxlength'=>32,
  );
  $form['fieldfinder_group_add']['weight']=array(
		'#type'=>'weight',
		'#title'=>t('Weight'),
		'#default_value'=>1,
  );
  $form['fieldfinder_group_add']['submit']=array(
		'#type'=>'submit',
		'#value'=>t('Add'),
		'#submit'=>array('fieldfinder_admin_group_form_submit'),
  );

  $form['#theme']='fieldfinder_admin_group_form';
  return $form;
}
function fieldfinder_admin_group_form_submit($form,&$form_state){
  $weight=$form_state['values']['weight'];
  $result=db_query("INSERT INTO {fieldfinder_group} (lid, groupname, weight) VALUES (%d, '%s', %d) ",$form_state['values']['lid'],$form_state['values']['groupname'],$weight);
  if ($result){
		drupal_set_message(t('Group has been saved!'));
  }
}
function theme_fieldfinder_admin_group_list_form(&$form) {
  $rows=array();
  $header=array(t('Name'),t('Weight'),t('Delete'),);
  $output='';
  foreach (element_children($form['gid']) as $key){
		$row=array();
		$row[]=drupal_render($form['groupname_'.$key]);
		$row[]=drupal_render($form['weight_'.$key]);
		$row[]=drupal_render($form['delete'][$key]);

		$rows[]=array(
		  'data'=>$row,
		  'class'=>'draggable',
		);
  }

  $output .= theme('table',$header, $rows,array('id'=>'fieldfinder-group-list-table'));
  drupal_add_tabledrag('fieldfinder-group-list-table','order','sibling','fieldfinder-group-list-weight');
  $output .= drupal_render($form);

  return $output;
}
function fieldfinder_admin_group_list_form(&$form_state,$lid){
  $results=db_query("SELECT * from {fieldfinder_group} WHERE lid = %d ORDER BY weight", $lid);
  $gids=array();
  
  while ($group = db_fetch_object($results)){
		$form['gid'][$group->gid]=array(
		  '#type'=>'hidden',
		  '#value'=>$group->gid,
		);
		$form['groupname_' .$group->gid]=array(
		  '#type'=>'textfield',
		  '#default_value'=>$group->groupname,
		);
		$form['weight_' .$group->gid]=array(
		  '#type'=>'weight',
		  '#default_value'=>$group->weight,
		  '#attributes'=>array(
				'class'=>'fieldfinder-group-list-weight',
		  ),
		);
		$gids[$group->gid]='';
  }


  $form['delete']=array(
		'#type'=>'checkboxes',
		'#options'=>$gids,
  );
  $form['lid']=array(
		'#type'=>'hidden',
		'#value'=>$lid,
  );
  $form['save']=array(
		'#type'=>'submit',
		'#value'=>t('Save Changes'),
		'#submit'=>array('fieldfinder_admin_group_list_form_submit'),
  );
  $form['#theme']='fieldfinder_admin_group_list_form';
  
  return $form;
}
function fieldfinder_admin_group_list_form_submit(&$form,&$form_state){
  /* 格式化表单数组 */
  $updates=array();
  foreach ($form_state['values'] as $key => $value){
		if (preg_match('!^[A-z]*_[0-9]+$!',$key)){
		  //filter[1]字段名,filter[0]gid
		  $filter=explode('_',$key);
		  $updates[$filter[1]][$filter[0]]=$value;
		}
		if ($key == 'delete'){
		  foreach ($value as $gid => $yesorno){
				$updates[$gid]['delete']=($yesorno == $gid)?'TRUE':'FALSE';
		  }
		}
  }
  /*进行更新或删除操作*/
  foreach ($updates as $gid => $value){
		if ($value['delete'] == 'TRUE') {
		  $result=db_query("DELETE FROM {fieldfinder_group} WHERE gid = %d AND lid = %d ",$gid,$form_state['values']['lid']);
		}else{
		  $result=db_query("UPDATE {fieldfinder_group} SET groupname = '%s', weight = '%s' WHERE gid = %d AND lid= %d ",$value['groupname'],$value['weight'],$gid,$form_state['values']['lid']);
		}
  }
  drupal_set_message(t('All changes have been saved!'));
}


function theme_fieldfinder_admin_field_edit_form(&$form){
  $rows=array();
  $header=array(t('Select'),t('Weight'),t('Group'),);
  $output='';
  foreach (element_children($form['select_field']) as $key){
		$row=array();
		$row[]=drupal_render($form['select_field'][$key]);
		$row[]=drupal_render($form['weight__'.$key]);
		$row[]=drupal_render($form[$key]);

		$rows[]=array(
		  'data'=>$row,
		  'class'=>'draggable',
		);
  }
  $output .= theme('table',$header,$rows,array('id'=>'fieldfinder-fields-edit-table'));
  drupal_add_tabledrag('fieldfinder-fields-edit-table','order','sibling','fieldfinder-fields-edit-weight');
  $output .= drupal_render($form);

  return $output;
}
function fieldfinder_admin_field_edit_form(&$form_state,$lid){
  $list=fieldfinder_get_lists_instance($lid);

  //初始化已存在的设置
  $fieldinfos=array();
  if ($list['field']){
		foreach ($list['field'] as $fieldinfo){
		  $fieldinfos[$fieldinfo->ccktypename]=array(
				'fid'=>$fieldinfo->fid,
				'lid'=>$lid,
				'gid'=>$fieldinfo->gid,
				'ccktypename'=>$fieldinfo->ccktypename,
				'weight'=>$fieldinfo->weight,
		  );
		}
  }
  $checkboxes=array();  //选择字段
  $checkboxesvalues=array();
  $checkboxesweight=array(); //字段的顺序
  $group=array();		  //选择分组
  $grouplist=array();
  $grouplist[0]=t('nogroup');
  if (!empty($list['group'])){
		foreach($list['group'] as $ingroup){
		  $grouplist[$ingroup->gid]=$ingroup->groupname;
		}
  }

  $form['lid']=array(
		'#type'=>'hidden',
		'#value'=>$lid,
  );

  $type_res=db_query("SELECT * FROM {node_type} WHERE type = '%s'",$list['base']->content_type);

  while ($type = db_fetch_object($type_res)){
		if ($type->has_title == '1'){
		  $checkboxes['title_label']=$type->title_label . "[title]";
		  if (!empty($fieldinfos['title_label'])){
				$form['title_label']=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>$fieldinfos['title_label']['gid'],
				);
				$form['weight__title_label']=array(
				  '#type'=>'weight',
				  '#default_value'=>$fieldinfos['title_label']['weight'],
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues['title_label']='title_label';
				$checkboxesweight['title_label']=$fieldinfos['title_label']['weight'];
		  }else{
				$form['title_label']=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>'0',
				);
				$form['weight__title_label']=array(
				  '#type'=>'weight',
				  '#default_value'=>'1',
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues['title_label']='0';
				$checkboxesweight['title_label']='1';
		  }
		}
		if ($type->has_body == '1'){
		  $checkboxes['body_label']=$type->body_label . "[body]";
		  if (!empty($fieldinfos['body_label'])){
				$form['body_label']=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>$fieldinfos['body_label']['gid'],
				);
				$form['weight__body_label']=array(
				  '#type'=>'weight',
				  '#default_value'=>$fieldinfos['body_label']['weight'],
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues['body_label']='body_label';
				$checkboxesweight['body_label']=$fieldinfos['body_label']['weight'];
		  }else{
				$form['body_label']=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>'0',
				);
				$form['weight__body_label']=array(
				  '#type'=>'weight',
				  '#default_value'=>'1',
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues['body_label']='0';
				$checkboxesweight['body_label']='1';
		  }
		}
  }
  $cck_res=db_query("SELECT * FROM {content_node_field_instance} WHERE type_name = '%s' ORDER BY weight ", $list['base']->content_type);
  while ($cck = db_fetch_object($cck_res)){
		if ($cck->widget_active == 1){
		  $checkboxes[$cck->field_name]=$cck->label . "[{$cck->field_name}]";
		  if (!empty($fieldinfos[$cck->field_name])){
				$form[$cck->field_name]=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>$fieldinfos[$cck->field_name]['gid'],
				);
				$form['weight__'.$cck->field_name]=array(
				  '#type'=>'weight',
				  '#default_value'=>$fieldinfos[$cck->field_name]['weight'],
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues[$cck->field_name]=$cck->field_name;
				$checkboxesweight[$cck->field_name]=$fieldinfos[$cck->field_name]['weight'];
		  }else{
				$form[$cck->field_name]=array(
				  '#type'=>'select',
				  '#options'=>$grouplist,
				  '#default_value'=>'0',
				);
				$form['weight__'.$cck->field_name]=array(
				  '#type'=>'weight',
				  '#default_value'=>'1',
				  '#attributes'=>array(
						'class'=>'fieldfinder-fields-edit-weight',
				  ),
				);
				$checkboxesvalues[$cck->field_name]='0';
				$checkboxesweight[$cck->field_name]='1';
		  }
		}
  }

  //排序字段
  asort($checkboxesweight);
  $checkboxesorder=array();
  foreach($checkboxesweight as $key=>$inweigt){
		$checkboxesorder[$key]=$checkboxes[$key];
  }

  $form['select_field']=array(
		'#type'=>'checkboxes',
		'#options'=>$checkboxesorder,
		'#default_value'=>$checkboxesvalues,
  );
  $form['save']=array(
		'#type'=>'submit',
		'#value'=>'Save',
		'#submit'=>array('fieldfinder_admin_field_edit_form_submit'),
  );

  return $form;
}
function fieldfinder_admin_field_edit_form_submit($form,&$form_state){
  $lid=$form_state['values']['lid'];//lid
  foreach ($form_state['values'] as $key=>$value){
		$ccktypename='';//ccktypename
		$operation = 'delete';
		$weight='';//weight
		$gid=0;//gid
		if ($key == 'title_label'){
		  $ccktypename=$key;
		  if ($form_state['values']['select_field'][$key] != '0'){
				$operation='update';
				$gid=$form_state['values'][$key];
		  }
		}else if ($key == 'body_label'){
		  $ccktypename=$key;
		  if ($form_state['values']['select_field'][$key] != '0'){
				$operation='update';
				$gid=$form_state['values'][$key];
		  }
		}else if (preg_match('!^field_!',$key)){
		  $ccktypename=$key;
		  if ($form_state['values']['select_field'][$key] != '0'){
				$operation='update';
				$gid=$form_state['values'][$key];
		  }
		}
		if (!empty($ccktypename)){
		  //判断当前字段是否存在于已有设置中,如果不存在,且$operation=update则进行插入操作；如果存在,且$operation=update,则进行更新操作；如果存在,且$operation=delete,则进行删除操作
		  $exist= db_query("SELECT fid from {fieldfinder_field} WHERE lid= %d AND ccktypename = '%s' ",$lid,$ccktypename);
		  $exist=db_result($exist);
		  $weight=$form_state['values']['weight__' . $ccktypename];
		  if (empty($exist)){
				//插入操作
				if ($operation == 'update'){
				  $result=db_query("INSERT INTO {fieldfinder_field} (lid, gid, ccktypename, weight) VALUES (%d, %d, '%s', %d) ",$lid,$gid,$ccktypename,$weight);
				}
		  }else{
				//更新
				if ($operation == 'update'){
				  $result=db_query("UPDATE {fieldfinder_field} SET gid=%d, weight=%d WHERE lid=%d AND ccktypename='%s' ",$gid,$weight,$lid,$ccktypename);
				}else if ($operation == 'delete'){
				  //删除
				  $result=db_query("DELETE FROM {fieldfinder_field} WHERE lid=%d AND ccktypename='%s' ",$lid,$ccktypename);
				}
		  }
		}
  }

  drupal_set_message(t('All changes have been saved'));
}

function fieldfinder_admin_del_form(&$form_state,$lid){
  $list=fieldfinder_get_lists_instance($lid);
   
  $form['lid']=array(
		'#type'=>'hidden',
		'#value'=>$lid,
  );
  $form['listname']=array(
		'#type'=>'hidden',
		'#value'=>$list['base']->listname,
  );
  $form['#submit']=array('fieldfinder_admin_del_form_submit');

  return confirm_form(
		$form, 
		t('Are you sure you want to delete this fieldfinder?'),
		'admin/build/fieldfinder',
		t("This action will delete <b> '{$list['base']->listname}' </b>, and this action cannot be undone.<br/>"), 
		'Delete',
		'Cancel',
		'confirm'
  );
}
function fieldfinder_admin_del_form_submit($form,&$form_state){
  $lid=$form_state['values']['lid'];
  if ($form_state['values']['op']=='Delete'){
		$result=db_query("DELETE FROM {fieldfinder_field} WHERE lid = %d ",$lid);
		$result=db_query("DELETE FROM {fieldfinder_term} WHERE lid = %d ",$lid);		
		$result=db_query("DELETE FROM {fieldfinder_group} WHERE lid = %d ",$lid);
		$result=db_query("DELETE FROM {fieldfinder_list} WHERE lid = %d ",$lid);
  }
  drupal_set_message(t("<b>'{$form_state['values']['listname']}'</b> has been delete!"));
  drupal_goto('admin/build/fieldfinder');

}

function fieldfinder_search_results(&$form_state){
  $post_values=$_REQUEST['values'];
  $lists_instance=fieldfinder_get_lists_instance($post_values['lid']);
  $cck_tablename='content_type_' . $lists_instance['base']->content_type;

  //每个字段的查询语句
  $query_wheres=array();
  $query_where='';
  foreach ($lists_instance['field'] as $fields){
		switch ($fields->ccktypename){
		  case 'title_label':
				if (!empty($post_values[$fields->fid])){
				  $query_wheres[$fields->ccktypename]=" n.title LIKE '%{$post_values[$fields->fid]}%' ";
				}
				break;
		  case 'body_label':
				break;
		  default:
				//根据content_fields有没有allowed_values来判断是用LIKE还是用等于
				if (!empty($post_values[$fields->fid])){
				  $cckfield=content_fields($fields->ccktypename);
				  if (empty($cckfield['allowed_values'])){
						$query_wheres[$fields->ccktypename]=" c.{$fields->ccktypename}_value LIKE '%{$post_values[$fields->fid]}%' ";
				  }else{
						$query_wheres[$fields->ccktypename]=" c.{$fields->ccktypename}_value = '{$post_values[$fields->fid]}' ";
				  }
				}
		}
  }

  //判断是否有查询条件
  if(!empty($query_wheres)){
		$query_where=implode(' AND ',$query_wheres);
		$query_where = ' WHERE ' . $query_where;
  }

  $query='SELECT DISTINCT n.nid FROM {node} n INNER JOIN {' . $cck_tablename . '} c ON n.nid = c.nid ' . $query_where;
  $results = pager_query($query,variable_get('default_nodes_main',10),0,null);

  //调用模板
  $output='';
  $out_nodes=array();
  while ($result = db_fetch_object($results)){
		$out_nodes[]=node_load($result->nid);
  }

  if (empty($out_nodes)){
		$output .= t('Your search yielded no results!');
  }else{
		//搜索结果的templates路径,若在主题文件夹中有模板,则调用,若不存在,则调用theme中的默认模板
		global $theme;
		$base=drupal_get_path('theme',$theme);
		$tpl_file='';
		if (file_exists($base . '/fieldfinder-lid--' . $lid . '-' . 'results.tpl.php')){
		  $tpl_file=$base . '/fieldfinder-lid--' . $lid . '-' . 'results.tpl.php';
		}else{
		  $tpl_file=drupal_get_path('module' , 'fieldfinder') . '/theme/fieldfinder-' . 'results.tpl.php';
		}

		include($tpl_file);
		$output .= theme('pager',NULL,50,0);
  }

  return $output;
}
